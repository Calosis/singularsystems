﻿//The text files "Universe" and "FlightPlan", can be found in the root "SingularSystems" solution folder.

//ASSUMPTIONS:
//1.I have taken the rate of colonization as 0.043 per MILLION square kilometers. I believe this keeps the question interesting as at 0.043 per sqkm it would take
//  almost 6 hours to colonise 50% of the smallest 1 mil sqkm planet, there by only allowing a maximum of four of the smallest planets if they're close by.
//2.I have assumed the optimum route is not to go back to a planet already visited.
//3.Due to the ambiguity in the question, I should state that while avoiding monsters and uninhabitable planets makes the question more linear, the question does offer the parameters one would 
//  need to follow in order to heada to and pass a space monster for a very dynamic problem, hence I've chosen not to avoid them and factor in the extra time required to get passed them.
//4.While 2a requested the maximum colonised space, 2b requested the largest number of habbitable planets. Innitially I planned two reports, but found they were the same route 99% of the time
//  and so I've chosen number of planets, as only one route can be chosen (adding another report may constitute overengineering as it is outside the scope of the question).

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingularSystemsCalculateRoute
{
    class Program
    {
        //For the sake of readability I have not put these two settings in the config.
        const int NumberOfFlightPlansPerHop = 2;//This determines how many routes will be spawned upon every hop. e.g. When set to 2, 2 routes (to the nearest and 2nd nearest neighbours) are created.
        const int MaxRoutesToCompute = 1000;//This determines the total number of routes that will be spawned.

        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            //********************max travel time to the next destination that will be considered is less than 30 minutes (or 1800 seconds). *********************
            //Because in 20 minutes I could get passed a space monster (if it's my nearest neighbour) and add another 10 minutes to get to it's nearest neighbour (assuming it's a habbitable planet)... alot of time wasted!

            List<FlightPlan> routes = new List<FlightPlan>();
            List<ObjectInSpace> objectsInSpaceList = await GetUniverseDetails();

            //Populate the distance to every other planet in the universe from the current one
            objectsInSpaceList.ForEach(x => x.Distance = CalculateDistance(objectsInSpaceList[0].X, objectsInSpaceList[0].Y, objectsInSpaceList[0].Z, x.X, x.Y, x.Z)); 
            var nearestObjects = objectsInSpaceList.OrderBy(x => x.Distance).ToList();//order them to find the closest ones

            routes.Add(new FlightPlan() { ObjectsVisited = new List<ObjectInSpace>(), ObjectsNotVisited = new List<ObjectInSpace>() });
            routes[0].ObjectsVisited.Add(nearestObjects[0]); //Add homeworld as a visited object as I'm starting here
            nearestObjects.Remove(nearestObjects[0]);
            routes[0].ObjectsNotVisited.AddRange(nearestObjects); //Add all objects I have not visited yet

            FlightPlan OptimizedRoute = (await ComputeAllRoutes(routes)).OrderByDescending(c => c.TotalPlanetsColonized).First();

            await WriteToFile(OptimizedRoute);
        }

        static async Task<List<FlightPlan>> ComputeAllRoutes(List<FlightPlan> routes)
        {
            for (int loop = 0; loop <= (routes.Count()-1) && loop <= MaxRoutesToCompute; loop++)
            {//Initially only a single route is passed to this method but a set number of new routes, indicated by "NumberOfFlightPlansPerHop" (default : 2), will be spawned 
                do//from every current route till the total number of routes reaches "MaxRoutesToCompute" (default : 1000)
                {
                    List<FlightPlan> currentRouteUpdateAndNewRoutes = await FindClosestHops(routes[loop]);//For each route we find and jump to the closest available objects in the universe
                    routes[loop] = currentRouteUpdateAndNewRoutes[0];//update the current route now that we've jumped to a new object
                    currentRouteUpdateAndNewRoutes.Remove(currentRouteUpdateAndNewRoutes[0]);//remove the current route from the list as it's info has already been updeated
                    routes.AddRange(currentRouteUpdateAndNewRoutes);//wee add in all the new routes that have been spawned
                } while (routes[loop].RouteCompleteInd == false);//continue these steps till said route has used as much of it's 24 hour window as it can
            }

            return routes;
        }

        static async Task<List<FlightPlan>> FindClosestHops(FlightPlan OriginalRoute)
        {
            List<FlightPlan> routesToConsider = new List<FlightPlan>();

            double speed = OriginalRoute.ObjectsNotVisited[0].Distance / 600; //10 minutes to the immediate neighbour gives us our speed from this planet, I then work out how long to the 2nd or 3rd neighbours etc.
            for (int loop = 0; loop <= (NumberOfFlightPlansPerHop - 1); loop++) //loop goes up to the number of new routes that should be spawned every time we hop to a new object in space
            {
                FlightPlan route = OriginalRoute;//the split occurs here, and branch of to the immediate or 2nd or 3rd neighbours etc. 
                var currentPlanet = route.ObjectsNotVisited[loop];//where each route heads off toward their respective planet

                var travelTime = currentPlanet.Distance / speed;
                if (currentPlanet is Monster)
                    travelTime *= 2;//double the travel time to dodge monsters

                if ((route.Time + travelTime) <= 86400) //checking if we can get there within 24hrs (this is left here for simplicity, instead of creating it's own function as it's only done twice)
                    route.Time += travelTime;
                else
                    route.RouteCompleteInd = true;

                if (currentPlanet.Habbitable)
                    route = await ColonisePlanet(route, (Planet)currentPlanet);//if it's habittable, cast the object and colonise it!
                else
                {
                    route.ObjectsVisited.Add(currentPlanet);//if it's not habittable i.e. it's uninhabittable or a monster, we move it from the not visited list to the visited one.
                    route.ObjectsNotVisited.Remove(currentPlanet);
                }

                route.ObjectsNotVisited.ForEach(c => c.Distance =
                (CalculateDistance(currentPlanet.X, currentPlanet.Y, currentPlanet.Z, c.X, c.Y, c.Z)));
                route.ObjectsNotVisited = route.ObjectsNotVisited.OrderBy(c => c.Distance).ToList();//Now that we're at a new planet we again have to find the closest objects in space that we havn't visited

                routesToConsider.Add(route);//this step is now complete and the route is then added to the list
            }

            return routesToConsider;
        }

        static async Task<FlightPlan> ColonisePlanet(FlightPlan CurrentFlightPlan, Planet PlanetToColonise)
        {
            double timeTakenToColonize = (PlanetToColonise.Space / 2 + 0.000001) * 0.043;//Here is the assumption of the speed as well as making sure I colonise 1 sqkm more than half to fulfil the prerequisite
            if ((CurrentFlightPlan.Time + timeTakenToColonize) <= 86400)//do I have enough time to colonise the planet
            {
                CurrentFlightPlan.Time += timeTakenToColonize;
                CurrentFlightPlan.TotalConquredSpace += PlanetToColonise.Space;
                CurrentFlightPlan.TotalPlanetsColonized++;
                CurrentFlightPlan.ObjectsVisited.Add(PlanetToColonise);//move planet to visited from not visited list
                CurrentFlightPlan.ObjectsNotVisited.Remove(PlanetToColonise);
            }
            else
                CurrentFlightPlan.RouteCompleteInd = true;

            return CurrentFlightPlan;
        }

        static double CalculateDistance(int fromPlanetX, int fromPlanetY, int fromPlanetZ, int toPlanetX, int toPlanetY, int toPlanetZ)
        {
            if ((fromPlanetX != toPlanetX) && (fromPlanetY != toPlanetY) && (fromPlanetZ != toPlanetZ)) //check that we're not trying to travel to the very planet we're on!
            {
                double distance = Math.Sqrt(
                Math.Pow(fromPlanetX - toPlanetX, 2)           //Pythagorean theorem used to calculate distance
                + Math.Pow(fromPlanetY - toPlanetY, 2)
                + Math.Pow(fromPlanetZ - toPlanetZ, 2));

                return distance;
            }
            else
                return 0;
        }

        static async Task<List<ObjectInSpace>> GetUniverseDetails()
        {
            List<ObjectInSpace> objectInSpaceList = new List<ObjectInSpace>();

            ObjectInSpace homePlanet = new Planet(123123991, 098098111, 456456999, true, 0, true); //Adding my home world

            objectInSpaceList.Add(homePlanet);

            foreach (string line in File.ReadAllLines("..\\..\\..\\Universe.txt"))
            {
                string[] variables = line.Split(',');
                if (bool.Parse(variables[4]))
                    objectInSpaceList.Add(new Monster( // Adding monsters
                            int.Parse(variables[0]),
                            int.Parse(variables[1]),
                            int.Parse(variables[2])
                            ));
                else
                    objectInSpaceList.Add(new Planet( // Adding the planets
                        int.Parse(variables[0]),
                        int.Parse(variables[1]),
                        int.Parse(variables[2]),
                        bool.Parse(variables[3]),
                        int.Parse(variables[5]),
                        false));
            }
            return objectInSpaceList;
        }

        public abstract class ObjectInSpace
        {
            public int X { get; set; }
            public int Y { get; set; }
            public int Z { get; set; }

            public double Distance { get; set; }
            public bool Habbitable { get; set; }
            public bool Colonized { get; set; }
        }


        public class Planet : ObjectInSpace
        {
            public Planet(int _X, int _Y, int _Z, bool _Habbitable, int _Space, bool _Colonized)
            {
                this.X = _X;
                this.Y = _Y;
                this.Z = _Z;
                this.Habbitable = _Habbitable;
                this.Space = _Space;
                this.Colonized = _Colonized;
            }
            public int Space { get; set; }
        }

        public class Monster : ObjectInSpace
        {
            public Monster(int _X, int _Y, int _Z)
            {
                this.X = _X;
                this.Y = _Y;
                this.Z = _Z;
            }
        }

        public class FlightPlan
        {
            public List<ObjectInSpace> ObjectsVisited { get; set; }
            public List<ObjectInSpace> ObjectsNotVisited { get; set; }
            public double Time { get; set; }
            public int TotalPlanetsColonized { get; set; }
            public int TotalConquredSpace { get; set; }
            public bool RouteCompleteInd { get; set; }
        }

        static async Task WriteToFile(FlightPlan OptimizedRoute) // Create report
        {
            using (var writer = new StreamWriter("..\\..\\..\\FlightPlan.txt"))
            {
                writer.WriteLine("**********************THE 24 HOUR COLONIZATION FLIGHT PLAN DEPARTING FROM HOME**********************");
                OptimizedRoute.ObjectsVisited.Remove(OptimizedRoute.ObjectsVisited[0]);//remove home planet
                int monsterCount = 0;
                int uninhabitablePlanetCount = 0;
                foreach (ObjectInSpace item in OptimizedRoute.ObjectsVisited)
                {
                    string detail = "";
                    if (item is Planet && item.Habbitable)
                        detail = string.Format("A colony with {0}sqkm of space.", ((Planet)item).Distance.ToString());
                    else if (item is Monster)
                    {
                        detail = "A monster we need to dodge. Beware!";
                        monsterCount++;
                    }
                    else
                    {
                        detail = "An uninhabbitable planet we'll have to go around.";
                        uninhabitablePlanetCount++;
                    }
                    writer.WriteLine(
                    string.Format("X : {0} | Y : {1} | Z : {2} | {3}",
                    item.X.ToString("0,0").PadLeft(11),
                    item.Y.ToString("0,0").PadLeft(11),
                    item.Z.ToString("0,0").PadLeft(11),
                    detail
                    ));
                }
                writer.WriteLine(
                    string.Format(" It will take us {0} hours, we'll colonize {1}sqkm on {2} planets, we'll have to dodge {3} space monsters and bypass {4} uninhabbitable planets",
                    (OptimizedRoute.Time / 3600),//convert to hours as the question specifies time in hours
                    OptimizedRoute.TotalConquredSpace,
                    OptimizedRoute.TotalPlanetsColonized,
                    monsterCount,
                    uninhabitablePlanetCount
                    ));
            }
        }
    }
}
